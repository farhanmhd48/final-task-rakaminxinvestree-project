package com.investree.demo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestingController {
    
    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void restTemplateSave() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "*/*");
        headers.set("Content-Type", "application/json");
        String bodyTesting = "{\n" + 
            "   \"content\": \"[{}]\", \n" +
            "   \"pageable\": \"{}\", \n" +
            "   \"totalPages\": \"2\", \n" +
            "   \"last\": \"false\", \n" +
            "   \"totalElements\": \"2\", \n" +
            "   \"size\": \"1\", \n" +
            "   \"number\": \"0\", \n" +
            "   \"sort\": \"{}\", \n" +
            "   \"first\": \"true\", \n" +
            "   \"numberOfElements\": \"1\", \n" +
            "   \"empty\": \"false\", \n" +
            "}";
        
        HttpEntity<String> entity = new HttpEntity<String>(bodyTesting, headers);
        ResponseEntity<String> exchange = restTemplate.exchange("http://localhost:8081/v1/transaksi", HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
        System.out.println("response = " + exchange.getBody());
    }
}
