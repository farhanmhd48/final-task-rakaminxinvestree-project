package com.investree.demo.controller;

import java.util.HashMap;

import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.investree.demo.model.Transaksi;
import com.investree.demo.repository.TransaksiRepository;
import com.investree.demo.view.TransaksiService;

@RestController
@RequestMapping("/v1/Transaksi")
public class TransaksiController {

    @Autowired
    public TransaksiService transaksiService;

    @Autowired
    public TransaksiRepository transaksiRepository;

    @PostMapping("")
    public ResponseEntity<Map> save(@RequestBody Transaksi objModel) {
        Map save = transaksiService.save(objModel);
        return new ResponseEntity<Map>(save, HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<Map> update(@RequestBody Transaksi objModel) {
        Map update = transaksiService.updateStatus(objModel);
        return new ResponseEntity<Map>(update, HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<Map> getListData(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam( required = false) String status ) {
        PageRequest show_data = PageRequest.of(page, size);
        Page<Transaksi> list = null;
        if(status != null) {
            transaksiRepository.getByStatus(status, show_data);
        } else {
            transaksiRepository.getAllData(show_data);
        }

        Map map = new HashMap();
        map.put("content", list);
        map.put("code", 200);
        map.put("status", "success");
        return new ResponseEntity<Map>(map, new HttpHeaders(), HttpStatus.OK);
    }

}
