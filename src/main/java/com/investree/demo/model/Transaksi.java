package com.investree.demo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "transaksi")
public class Transaksi implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "id_peminjam")
    private Long id_peminjam;
    @Column(name = "id_meminjam")
    private Long id_meminjam;
    @Column(name = "tenor")
    private Integer tenor;
    @Column(name = "total_pinjaman")
    private Double total_pinjaman;
    @Column(name = "bunga_persen")
    private Double bunga_persen;
    @Column(name = "status")
    private String status;

}
