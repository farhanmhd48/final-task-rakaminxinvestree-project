package com.investree.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "paymenthistory")
public class PaymentHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "id_transaksi")
    private Long id_transaksi;
    @Column(name = "pembayaran_ke")
    private Integer pembayaran_ke;
    @Column(name = "jumlah")
    private Double jumlah;
    @Column(name = "bukti_pembayaran")
    private String bukti_pembayaran;
    
}
