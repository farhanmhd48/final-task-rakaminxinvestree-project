package com.investree.demo.repository;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.investree.demo.model.Transaksi;

@Repository
public interface TransaksiRepository extends PagingAndSortingRepository<Transaksi, Long> {

    @Query("SELECT t FROM Transaksi WHERE t.id = :id")
    public Transaksi getByID(@Param("id") Long id);

    @Query("SELECT t FROM Transaksi WHERE t.id_peminjam = :id_peminjam")
    public Transaksi getByIDPeminjam(@Param("id_peminjam") Long id_peminjam);

    @Query("SELECT t FROM Transaksi WHERE t.id_meminjam = :id_meminjam")
    public Transaksi getByIDMeminjam(@Param("id_meminjam") Long id_meminjam);

    @Query("SELECT t FROM Transaksi WHERE t.status = :status")
    public Transaksi getByStatus(@Param("status") String status);

    @Query("SELECT t FROM Transaksi t")
    Page<Transaksi> getAllData(Pageable pageable);
    
}
