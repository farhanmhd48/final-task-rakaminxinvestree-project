package com.investree.demo.repository;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.investree.demo.model.PaymentHistory;

@Repository
public interface PaymenHistoryRepository extends PagingAndSortingRepository<PaymentHistory, Long> {
    
    @Query("SELECT ph FrOM PaymentHistory ph WHERE ph.id = :id")
    public PaymentHistory getByID(@Param("id") Long id);

    @Query("SELECT ph FrOM PaymentHistory ph WHERE ph.id_transaksi = :id_transaksi")
    public PaymentHistory getByIDTransaksi(@Param("id_transaksi") Long id_transaksi);

    @Query("SELECT ph FrOM PaymentHistory ph")
    Page<PaymentHistory> getAllData(Pageable pageable);

}
