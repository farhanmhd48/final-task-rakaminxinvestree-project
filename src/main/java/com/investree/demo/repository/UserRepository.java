package com.investree.demo.repository;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.investree.demo.model.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    
    @Query("SELECT u FROM User u WHERE u.id = :id")
    public User getByID(@Param("id") Long id);

    @Query("SELECT u FROM User u WHERE u.username = :username")
    public User getByUsername(@Param("username") String username);

    @Query("SELECT u FROM User u")
    Page<User> getAllData(Pageable pageable);

}
