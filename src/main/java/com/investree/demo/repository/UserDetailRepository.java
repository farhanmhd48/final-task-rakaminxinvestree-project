package com.investree.demo.repository;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.investree.demo.model.UserDetail;

@Repository
public interface UserDetailRepository extends PagingAndSortingRepository<UserDetail, Long>{
    
    @Query("SELECT ud FROM UserDetail ud WHERE ud.id = :id")
    public UserDetail getByID(@Param("id") Long id);

    @Query("SELECT ud FROM UserDetail ud WHERE ud.id_user = :id_user")
    public UserDetail getByIDUser(@Param("id_user") Long id_user);

    @Query("SELECT ud FROM UserDetail ud WHERE ud.nama = :nama")
    public UserDetail getByID(@Param("nama") String nama);

    @Query("SELECT ud FROM UserDetail ud")
    Page<UserDetail> getAllData(Pageable pageable);

}
