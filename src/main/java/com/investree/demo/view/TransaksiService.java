package com.investree.demo.view;

import org.hibernate.mapping.Map;

import com.investree.demo.model.Transaksi;

public interface TransaksiService {
    public Map save(Transaksi obj);
    public Map updateStatus(Transaksi obj);
}
